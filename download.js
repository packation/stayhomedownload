const puppeteer = require('puppeteer');

const url = process.argv[2]

fetch_download_url =  (async () => {
  const browser = await puppeteer.launch({
//    headless: false
  });
  const page = await browser.newPage();
  await page.goto(url);
//  await page.waitForFunction(()=>('quality_1080p' in window || 'quality_720p' in window || 'quality_480p' in window ))   
  await page.waitForFunction(()=>('media_0' in window)) 
  const download_url =  await page.evaluate(()=>{
    //return(window.quality_1080p || window.quality_720p || window.quality_480p);
    return(window.media_0);
  });  
  await browser.close();
  return download_url;
});
(async ()=>{
  const download_url = await fetch_download_url();
  process.stdout.write(download_url);
  process.exit(0);
})();

